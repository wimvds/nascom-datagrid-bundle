# NascomDataGridBundle

This bundle provides helpers that ease the creation of simple data grids.

*NOTE*: This is a pre release bundle, things *will* change and might break, once it is stable it will get a version
branch and it will possibly move to GitHub.

For license information, see: [LICENSE](LICENSE)

If you want to know how to use this bundle in your own projects, see [usage documentation](Resources/doc/usage.md)
