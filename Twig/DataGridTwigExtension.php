<?php

namespace Nascom\DataGridBundle\Twig;

use Kunstmaan\AdminListBundle\Service\ExportService;
use Nascom\DataGridBundle\DataGrid\DataGridInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\CompiledRoute;
use Kunstmaan\AdminListBundle\AdminList\AdminList;

/**
 * DataGridTwigExtension
 */
class DataGridTwigExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * {@inheritdoc}
     */
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('datagrid', array($this, 'renderDataGridWidget'), array('is_safe' => array('html'))),
        );
    }

    /**
     * Renders the HTML for a datagrid
     *
     * Example usage in Twig:
     *
     *     {{ datagrid(datagrid) }}
     *
     * @param DataGridInterface $datagrid
     *
     * @return string The html markup
     */
    public function renderDataGridWidget(DataGridInterface $datagrid)
    {
        $template = $this->environment->loadTemplate('NascomDataGridBundle:Twig:datagrid_widget.html.twig');

        return $template->render(array(
            'datagrid' => $datagrid
        ));
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'nascom_datagrid_twig_extension';
    }
}
