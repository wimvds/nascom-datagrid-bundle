# Usage

## Installation

Add the following to your composer.json file :

```
    ...
    "repositories": [
        ...
        {
            "type": "vcs",
            "url": "https://wimvds@bitbucket.org/wimvds/nascom-datagrid-bundle.git"
        }
    ],
    "require": {
        ...
        "nascom/datagrid-bundle": "^1.0@dev"
    }
```

Add the datagrid bundle (and pagerfanta bundle) to your app/AppKernel.php :

```php
    ...
    public function registerBundles()
    {
        $bundles = array(
            ...
            new WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle(),
            new Nascom\DataGridBundle\NascomDataGridBundle(),
            ...
```

Run ```composer update nascom/datagrid-bundle```


## Generate a datagrid

Creating a new datagrid is pretty straightforward. You create a new DataGrid object and attach some fields to it. The
fields will be rendered in the order they are provided (left to right).

```php
    $dataGrid = new DataGrid();
    $dataGrid
        ->addField('email', 'user.email.label', 'u.email')
        ->addField('firstName', 'user.first_name.label', 'u.firstName')
        ->addField('lastName', 'user.last_name.label', 'u.lastName')
        ->addField('address', 'user.address.label', null, ':Common:address.html.twig');
```

The ```addField``` takes up to 4 parameters :

* ```fieldName``` : the property of the object you wish to display. This can be a scalar column are even an object reference
    if the object has a __toString method or you provide a custom template to render its contents.

* ```label``` : the label that should be displayed as header for this field in the grid. All labels will be translated in the
    default Twig template that is provided.

* ```sortableField``` : by default this is null. If you set it the field will be sortable and the expression you enter here
    will be used to sort the data source.

* ```template``` : by default this is null. If you set it this template will be used to render the field value. A template
    receives the current item, the fieldName and the raw value of the property.


## Add some actions

You can add custom actions to the grid as follows :
```php
    $dataGrid
        ->addItemAction(new SimpleItemAction(function($item) { return ['route' => 'my_route_name', 'params' => ['id' => $item->getId()]] }, 'label', 'icon'))
        ->addListAction(new SimpleListAction(['route' => 'my_route_name', 'params' => ['id' => $item->getId()]], 'label', 'icon'));
```

As you see there are currently 2 basic action types :

* Item actions are added at the end of every line (ie. per item), they can be used to provide an link to edit or delete
    an item in the grid. All item actions should implement the ItemActionInterface.

* List actions are global (non-bulk) actions for the grid, they can be used to provide a link to create a new item.
    All list actions should implement the ListActionInterface.


## Generate a datasource

Let's assume you have a Doctrine EntityRepository that you want to use as the data source for your grid :

```php
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class UserRepository
 * @package Nascom\Core\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @return QueryBuilder
     */
    public function getUserListQueryBuilder()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->orderBy('u.email');

        return $qb;
    }
}
```

Attaching this repository as an ORMDataSource is plain and simple :

```php
    $dataSource = new ORMDataSource($this->repository->getUserListQueryBuilder());
    $dataSource->bindRequest($request);

    $dataGrid->setDataSource($dataSource);
```

The ```bindRequest``` call above is optional, but it can be used to pass the current http request parameters into the
proper data source attributes (currently these parameters are limited to the page number, the sort column and the sort
direction).


## Render the datagrid in your Twig template

First of all you have to pass the datagrid you created to your Twig template :
```php
    return $this->templating->renderResponse(
        '/Admin/User/list.html.twig',
        [
            'datagrid' => $dataGrid,
        ]
    );
```

And in your Twig template make sure you render the datagrid using :

```php
{{ datagrid(datagrid) }}
```

If you would like to use the default javascripts for column ordering, make sure you include the provided datagrid.js
file in your Twig template :

```php
{% javascripts '@NascomDataGridBundle/ui/js/datagrid.js' %}
    <script type="text/javascript" rel="javascript" src="{{ asset(asset_url) }}"></script>
{% endjavascripts %}
```

Or alternatively you can add it to a Gruntfile (or your task runner of choice) as an extra resource and include the
result in your template.

That's it!
