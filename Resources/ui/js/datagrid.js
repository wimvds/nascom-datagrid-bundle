var nascom = nascom || {};

nascom.datagrid = {};

(function ($) {

    nascom.datagrid.sort = (function(window, undefined) {

        var init,
            goToUrl;

        init = function() {
            $('.js-datagrid-sort').on('click', function() {
                goToUrl($(this));
            });
        };

        goToUrl = function($this) {
            window.location = $this.data('sort-url');
        };

        return {
            init: init
        };

    } (window));

    nascom.datagrid.init = function () {
        nascom.datagrid.sort.init();
    };

    // Document ready
    $(nascom.datagrid.init);

})(jQuery);
