<?php

namespace Nascom\DataGridBundle\DataGrid;

/**
 * Class DataGridField
 * @package Nascom\DataGridBundle\DataGrid
 */
class DataGridField
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $sortableField;

    /**
     * @var null|string
     */
    private $template;

    /**
     * @param string $name
     * @param string $label
     * @param string $sortableField
     * @param string $template
     */
    public function __construct($name, $label, $sortableField = null, $template = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->sortableField = $sortableField;
        $this->template = $template;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return boolean
     */
    public function isSortable()
    {
        return !is_null($this->sortableField);
    }

    /**
     * @return null|string
     */
    public function getSortableField()
    {
        return $this->sortableField;
    }

    /**
     * @return null|string
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
