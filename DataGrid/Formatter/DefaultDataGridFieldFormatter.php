<?php

namespace Nascom\DataGridBundle\DataGrid\Formatter;

use Doctrine\ORM\PersistentCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class DataGridFieldFormatter
 * @package Nascom\DataGridBundle\DataGrid
 */
class DefaultDataGridFieldFormatter implements DataGridFieldFormatterInterface
{
    /**
     * @param mixed $item
     * @param string $fieldName
     *
     * @return mixed
     */
    public function format($item, $fieldName)
    {
        $value = $this->getRawValue($item, $fieldName);

        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        if ($value instanceof \DateTime) {
            return $value->format('Y-m-d H:i:s');
        }

        if ($value instanceof PersistentCollection) {
            $results = '';
            /* @var Object $entry */
            foreach ($value as $entry) {
                $results[] = (string)$entry;
            }
            if (empty($results)) {
                return '';
            }

            return implode(', ', $results);
        }

        if (is_array($value)) {
            return implode(', ', $value);
        } else {
            return $value;
        }
    }

    /**
     * @param mixed $item
     * @param string $fieldName
     *
     * @return mixed
     */
    public function getRawValue($item, $fieldName)
    {
        if (is_array($item)) {
            if (isset($item[$fieldName])) {
                return $item[$fieldName];
            } else {
                return '';
            }
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        return $accessor->getValue($item, $fieldName);
    }
}
