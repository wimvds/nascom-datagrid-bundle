<?php

namespace Nascom\DataGridBundle\DataGrid\Formatter;

/**
 * Interface DataGridFieldFormatterInterface
 * @package Nascom\DataGridBundle\DataGrid\Formatter
 */
interface DataGridFieldFormatterInterface
{
    /**
     * @param mixed $item
     * @param string $fieldName
     *
     * @return mixed
     */
    public function format($item, $fieldName);

    /**
     * @param mixed $item
     * @param string $fieldName
     *
     * @return mixed
     */
    public function getRawValue($item, $fieldName);
}
