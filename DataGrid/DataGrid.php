<?php

namespace Nascom\DataGridBundle\DataGrid;

use Nascom\DataGridBundle\DataGrid\Actions\ItemActionInterface;
use Nascom\DataGridBundle\DataGrid\Actions\ListActionInterface;
use Nascom\DataGridBundle\DataGrid\DataSource\DataSourceInterface;
use Nascom\DataGridBundle\DataGrid\Formatter\DataGridFieldFormatterInterface;
use Nascom\DataGridBundle\DataGrid\Formatter\DefaultDataGridFieldFormatter;

class DataGrid implements DataGridInterface
{
    /**
     * @var DataGridField[]
     */
    protected $fields = [];

    /**
     * @var DataGridField[]
     */
    protected $sortableFields = [];

    /**
     * @var ItemActionInterface[]
     */
    protected $itemActions = [];

    /**
     * @var ListActionInterface[]
     */
    protected $listActions = [];

    /**
     * @var DataSourceInterface
     */
    protected $dataSource;

    /**
     * @var DataGridFieldFormatterInterface
     */
    protected $formatter;

    /**
     * @param DataGridFieldFormatterInterface $formatter
     */
    public function __construct(DataGridFieldFormatterInterface $formatter = null)
    {
        if (null === $formatter) {
            $formatter = new DefaultDataGridFieldFormatter();
        }
        $this->formatter = $formatter;
    }

    /**
     * @param string $name The field name
     * @param string $label The field label
     * @param string $sortableField The name of the field (including alias) if the field is sortable
     * @param string $template The template
     *
     * @return DataGrid
     */
    public function addField($name, $label, $sortableField = null, $template = null)
    {
        $this->fields[] = new DataGridField($name, $label, $sortableField, $template);

        return $this;
    }

    /**
     * @return DataGridField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return DataGridField[]
     */
    public function getSortableFields()
    {
        if (empty($this->sortableFields)) {
            $this->sortableFields = [];
            foreach ($this->getFields() as $field) {
                if ($field->isSortable()) {
                    $this->sortableFields[$field->getName()] = $field->getSortableField();
                }
            }
        }

        return $this->sortableFields;
    }

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function isSortable($fieldName = null)
    {
        if (is_null($fieldName)) {
            return count($this->getSortableFields()) > 0;
        }

        return in_array($fieldName, array_keys($this->getSortableFields()));
    }

    /**
     * @param string $fieldName
     *
     * @return string
     */
    public function getSortableField($fieldName = null)
    {
        if ($this->isSortable($fieldName)) {
            return $this->sortableFields[$fieldName];
        }

        return '';
    }

    /**
     * @param ItemActionInterface $itemAction
     *
     * @return DataGrid
     */
    public function addItemAction(ItemActionInterface $itemAction)
    {
        $this->itemActions[] = $itemAction;

        return $this;
    }

    /**
     * @return ItemActionInterface[]
     */
    public function getItemActions()
    {
        return $this->itemActions;
    }

    /**
     * @return boolean
     */
    public function hasItemActions()
    {
        return !empty($this->itemActions);
    }

    /**
     * @param ListActionInterface $listAction
     *
     * @return DataGrid
     */
    public function addListAction(ListActionInterface $listAction)
    {
        $this->listActions[] = $listAction;

        return $this;
    }

    /**
     * @return ListActionInterface[]
     */
    public function getListActions()
    {
        return $this->listActions;
    }

    /**
     * @return boolean
     */
    public function hasListActions()
    {
        return !empty($this->listActions);
    }

    /**
     * @param array|object $item The item
     * @param string $fieldName The field name
     *
     * @return mixed
     */
    public function getValue($item, $fieldName)
    {
        return $this->formatter->format($item, $fieldName);
    }

    /**
     * @param array|object $item The item
     * @param string $fieldName The field name
     *
     * @return mixed
     */
    public function getRawValue($item, $fieldName)
    {
        return $this->formatter->getRawValue($item, $fieldName);
    }

    /**
     * @return DataSourceInterface
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }

    /**
     * @param DataSourceInterface $dataSource
     * @return $this
     */
    public function setDataSource(DataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;

        return $this;
    }
}
