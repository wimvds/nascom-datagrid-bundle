<?php

namespace Nascom\DataGridBundle\DataGrid\Actions;

/**
 * A list action can be used to configure actions which can be executed on the whole list.
 */
interface ListActionInterface
{
    /**
     * @return array
     */
    public function getUrl();

    /**
     * @return string
     */
    public function getLabel();

    /**
     * @return string
     */
    public function getIcon();

    /**
     * @return array
     */
    public function getAttributes();

    /**
     * @return string
     */
    public function getTemplate();
}
