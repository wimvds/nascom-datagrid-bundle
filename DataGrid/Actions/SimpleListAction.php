<?php

namespace Nascom\DataGridBundle\DataGrid\Actions;

/**
 * The simple list action is a default implementation of the list action interface, this can be used
 * in very simple use cases.
 */
class SimpleListAction implements ListActionInterface
{
    /**
     * @var array
     */
    private $url;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $label;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var null|string
     */
    private $template;

    /**
     * @param array $url
     * @param string $label
     * @param string $icon
     * @param array $attributes
     * @param string $template
     */
    public function __construct(array $url, $label, $icon = null, $attributes = [], $template = null)
    {
        $this->url = $url;
        $this->label = $label;
        $this->icon = $icon;
        $this->attributes = $attributes;
        $this->template = $template;
    }

    /**
     * @return array
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
