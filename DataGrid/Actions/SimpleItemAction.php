<?php

namespace Nascom\DataGridBundle\DataGrid\Actions;

/**
 * The simple item action is a default implementation of the item action interface, this can be used
 * in very simple use cases.
 */
class SimpleItemAction implements ItemActionInterface
{
    /**
     * @var callable
     */
    private $routeGenerator;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $label;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @param callable $routeGenerator The generator used to generate the url of an item, when generating the item will
     *                                 be provided.
     * @param string $label The label
     * @param string $icon The icon
     * @param array|callable $attributes Extra attributes used to render this item (ie. data attributes)
     * @param string $template The template
     */
    public function __construct($routeGenerator, $label, $icon = null, $attributes = [], $template = null)
    {
        $this->routeGenerator = $routeGenerator;
        $this->label = $label;
        $this->icon = $icon;
        $this->attributes = $attributes;
        $this->template = $template;
    }

    /**
     * @var null|string
     */
    private $template;

    /**
     * @param mixed $item
     *
     * @return string
     */
    public function getUrl($item)
    {
        $routeGenerator = $this->routeGenerator;
        if (is_callable($routeGenerator)) {
            return $routeGenerator($item);
        }

        return null;
    }

    /**
     * @param mixed $item
     *
     * @return string
     */
    public function getIcon($item)
    {
        return $this->icon;
    }

    /**
     * @param mixed $item
     *
     * @return string
     */
    public function getLabel($item)
    {
        return $this->label;
    }

    /**
     * @param mixed $item
     *
     * @return array
     */
    public function getAttributes($item)
    {
        $attributes = $this->attributes;
        if (is_callable($attributes)) {
            return $attributes($item);
        }

        return $attributes;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }
}
