<?php

namespace Nascom\DataGridBundle\DataGrid\Actions;

/**
 * An item action can be used to configure custom actions on a listed item.
 */
interface ItemActionInterface
{
    /**
     * @param mixed $item
     *
     * @return array
     */
    public function getUrl($item);

    /**
     * @param mixed $item
     *
     * @return string
     */
    public function getLabel($item);

    /**
     * @param mixed $item
     *
     * @return string
     */
    public function getIcon($item);

    /**
     * @return array
     */
    public function getAttributes($item);

    /**
     * @return string
     */
    public function getTemplate();
}
