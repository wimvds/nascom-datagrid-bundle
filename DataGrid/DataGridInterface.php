<?php

namespace Nascom\DataGridBundle\DataGrid;

use Nascom\DataGridBundle\DataGrid\Actions\ItemActionInterface;
use Nascom\DataGridBundle\DataGrid\Actions\ListActionInterface;
use Symfony\Component\HttpFoundation\Request;

interface DataGridInterface
{
    /**
     * @param string $name
     * @param string $label
     * @param boolean $sortable
     * @param string $template
     * @return mixed
     */
    public function addField($name, $label, $sortable, $template = null);

    /**
     * @return DataGridField[]
     */
    public function getFields();

    /**
     * @return DataGridField[]
     */
    public function getSortableFields();

    /**
     * @param string $fieldName
     *
     * @return string
     */
    public function getSortableField($fieldName = null);

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function isSortable($fieldName);

    /**
     * @param ItemActionInterface $itemAction
     */
    public function addItemAction(ItemActionInterface $itemAction);

    /**
     * @return ItemActionInterface[]
     */
    public function getItemActions();

    /**
     * @return boolean
     */
    public function hasItemActions();

    /**
     * @param ListActionInterface $listAction
     *
     * @return DataGrid
     */
    public function addListAction(ListActionInterface $listAction);

    /**
     * @return ListActionInterface[]
     */
    public function getListActions();

    /**
     * @return boolean
     */
    public function hasListActions();

    /**
     * @param array|object $item The item
     * @param string $fieldName The field name
     *
     * @return mixed
     */
    public function getValue($item, $fieldName);
}
