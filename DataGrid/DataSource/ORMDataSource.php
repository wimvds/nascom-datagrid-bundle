<?php

namespace Nascom\DataGridBundle\DataGrid\DataSource;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class ORMDataSource
 * @package Nascom\DataGridBundle\DataGrid\DataSource
 */
class ORMDataSource extends AbstractDataSource
{
    /**
     * @var Query
     */
    private $query = null;

    /**
     * @var Pagerfanta
     */
    private $pagerfanta = null;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->getPager()->getNbResults();
    }

    /**
     * @return array|\Traversable
     */
    public function getItems()
    {
        return $this->getPager()->getCurrentPageResults();
    }

    /**
     * @return Pagerfanta
     */
    public function getPager()
    {
        if (is_null($this->pagerfanta)) {
            $adapter = new DoctrineORMAdapter($this->getQuery());
            $this->pagerfanta = new Pagerfanta($adapter);
            $this->pagerfanta->setNormalizeOutOfRangePages(true);
            $this->pagerfanta->setMaxPerPage($this->getPerPage());
            $this->pagerfanta->setCurrentPage($this->getPage());
        }

        return $this->pagerfanta;
    }

    /**
     * @return Query|null
     */
    private function getQuery()
    {
        if (is_null($this->query)) {
            $queryBuilder = $this->getQueryBuilder();

            // Apply sorting
            if (!empty($this->orderBy)) {
                $orderBy = $this->orderBy;
                $queryBuilder->orderBy($orderBy, ($this->orderDirection == 'DESC' ? 'DESC' : 'ASC'));
            }

            $this->query = $queryBuilder->getQuery();
        }

        return $this->query;
    }

    /**
     * @return QueryBuilder
     */
    private function getQueryBuilder()
    {
        return $this->queryBuilder;
    }
}
