<?php

namespace Nascom\DataGridBundle\DataGrid\DataSource;

use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;

interface DataSourceInterface
{
    /**
     * Return current page.
     *
     * @return int
     */
    public function getPage();

    /**
     * Return current sorting column.
     *
     * @return string
     */
    public function getOrderBy();

    /**
     * Return current sorting direction.
     *
     * @return string
     */
    public function getOrderDirection();

    /**
     * @return int
     */
    public function getPerPage();

    /**
     * @param int $perPage
     */
    public function setPerPage($perPage);

    /**
     * @param int $page
     */
    public function setPage($page);

    /**
     * @param string $orderBy
     */
    public function setOrderBy($orderBy);

    /**
     * @param string $orderDirection
     */
    public function setOrderDirection($orderDirection);

    /**
     * @return int
     */
    public function getCount();

    /**
     * @return array|\Traversable
     */
    public function getItems();

    /**
     * @return Pagerfanta
     */
    public function getPager();

    /**
     * @param Request $request
     */
    public function bindRequest(Request $request);
}
