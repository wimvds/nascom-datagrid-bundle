<?php

namespace Nascom\DataGridBundle\DataGrid\DataSource;

use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractDataSource
 * @package Nascom\DataGridBundle\DataGrid\DataSource
 */
abstract class AbstractDataSource implements DataSourceInterface
{
    /**
     * @var int
     */
    protected $page = 1;

    /**
     * @var int
     */
    protected $perPage = 10;

    /**
     * @var string
     */
    protected $orderBy = '';

    /**
     * @var string
     */
    protected $orderDirection = '';

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return AbstractDataSource
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     * @return AbstractDataSource
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     * @return AbstractDataSource
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderDirection()
    {
        return $this->orderDirection;
    }

    /**
     * @param string $orderDirection
     * @return AbstractDataSource
     */
    public function setOrderDirection($orderDirection)
    {
        $this->orderDirection = $orderDirection;

        return $this;
    }

    /**
     * @param Request $request
     */
    public function bindRequest(Request $request)
    {
        $this->page = $request->query->getInt('page', 1);

        // Only allow alphanumeric, _ and . in orderBy parameter!
        $this->orderBy = preg_replace('/[^[a-zA-Z0-9\_\.]]/', '', $request->query->get('orderBy', ''));
        $this->orderDirection = $request->query->getAlpha('orderDirection', '');
    }

    /**
     * @return int
     */
    abstract public function getCount();

    /**
     * @return array|\Traversable
     */
    abstract public function getItems();

    /**
     * @return Pagerfanta
     */
    abstract public function getPager();
}
